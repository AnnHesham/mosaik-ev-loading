
"""
This module contains EVs' load model.

"""
import arrow
from datetime import datetime, timedelta
import numpy as np
from simulators.ws2017_pv_model import PVpanel
import math

DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'

class Model(PVpanel):
    """Simple model that calculates the state of charge and power of EV."""

    def __init__(self, bat_cap_kwh, wallbox_kw, bat_limit, eff, mode, soc_old, start_date = None):
        self.bat_cap_kwh = bat_cap_kwh
        self.wall_box_kw = wallbox_kw
        self.bat_limit = bat_limit
        self.eff = eff
        self.mode = mode
        self.soc = soc_old
        self.time = (((((self.bat_limit - self.soc) * self.bat_cap_kwh) / self.bat_limit) /
                      (self.wall_box_kw * (self.eff / 100))) * 3600)

        if start_date is None:
            raise RuntimeError('start_date has to be given as sting!')
        else:
            self.date = arrow.get(start_date, DATE_FORMAT)


    def step_size(self, step_size):
        """Advance the current model time"""
        self.date = self.date.replace(seconds=step_size)


    def state_of_charge(self, lost_energy_kwh, car_plugged_in, p, step_size):
        """Calculate the SOC"""

        if self.mode == 'without_controller':
            step_soc = ((((self.wall_box_kw * (self.eff / 100) * step_size) / 3600) / self.bat_cap_kwh) * 100)
        elif self.mode == 'with_controller':
            step_soc = (((((self.power(lost_energy_kwh, car_plugged_in, p, step_size)) *
                           (self.eff / 100) * step_size) / 3600) / self.bat_cap_kwh) * 100)
        else:
            raise ValueError('mode should be either "with_controller" or "without_controller"')


        if (car_plugged_in == 1) and (lost_energy_kwh > 0):
            self.soc = (((self.bat_cap_kwh - lost_energy_kwh) / self.bat_cap_kwh) * 100) - (100 -self.soc)
            if self.soc > self.bat_limit:
                self.soc = self.bat_limit
        elif (car_plugged_in == 1) and (lost_energy_kwh == 0):
            self.soc = self.soc + step_soc
            if self.soc > self.bat_limit:
                self.soc = self.bat_limit
        elif (car_plugged_in == 0) and (lost_energy_kwh == 0):
            return self.soc
        return self.soc


    def power(self, lost_energy_kwh, car_plugged_in, p, step_size):
        """Calculate the  power"""

        if self.mode == 'without_controller':
            power = 0
            if (car_plugged_in == 1) and (lost_energy_kwh > 0):
                self.time = ((lost_energy_kwh / (self.wall_box_kw * (self.eff / 100))) * 3600) + self.time
                power = self.wall_box_kw
            elif (car_plugged_in == 1) and (lost_energy_kwh == 0):
                self.time = self.time - step_size
                if self.time < 0:
                    self.time = 0
                    power = 0
                elif self.time > 0:
                    power = self.wall_box_kw
            elif (car_plugged_in == 0) and (lost_energy_kwh == 0):
                power = 0
        elif self.mode == 'with_controller':
            if (-p > 0) and (car_plugged_in == 1):
                power = -p
            elif (-p > self.wall_box_kw) and (-p > 0):
                power = self.wall_box_kw
            else:
                power = 0
        else:
            raise ValueError('mode should be either "with_controller" or "without_controller"')
        return power

