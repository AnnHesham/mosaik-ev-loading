
# Import Python packages necessary for the example:
import mosaik
from mosaik.util import connect_many_to_one, connect_randomly

# Connecting information for all components employed in this scenario:
sim_config = {
     'PyPower': {
        'python': 'mosaik_pypower.mosaik:PyPower'
    },
    'CSV': {
        'python': 'mosaik_csv:CSV'
    },
    'DB': {
        'cmd': 'mosaik-hdf5 %(addr)s'
    },
    'EV': {
        'python': 'EV_charger_sim:EVSim',
    },
    'PV': {
        'python': 'simulators.ws2017_pv_simulator:PvAdapter'
    },
    'Ctrl': {
        'python': 'controller_sim:ControllerSim',
    }
}


# Parameter values for the scenario:
END = 4 * 24 * 60 * 60
START = '2014-01-01 00:00:00'
PROFILE_DATA = 'data/mprofiles.csv'
User_behaviour = 'data/behaviour.csv'
GRID_FILE = 'data/demo_lv_grid.json'
SOL_DATA = 'data/solar_data.csv'


# The world object organizes a mosaik scenario:
world = mosaik.World(sim_config)


# Initializing the simulators:
pypower = world.start('PyPower', step_size=15*60)
hhsim = world.start('CSV', sim_start=START, datafile=PROFILE_DATA)
evsim = world.start('EV', start_date=START, step_size=15*60)
ubsim_energy = world.start('CSV', sim_start=START, datafile=User_behaviour)
ubsim_plug = world.start('CSV', sim_start=START, datafile=User_behaviour)
DNIdata = world.start('CSV', sim_start=START, datafile=SOL_DATA)
pvsim = world.start('PV', start_date=START, step_time=15*60)


# Instantiating the model entities:
grid = pypower.Grid(gridfile=GRID_FILE).children
houses = hhsim.House.create(36)

# Filtering for specific entities from the grid composite model:
nodes = [element for element in grid if 'node' in element.eid]

ev = evsim.EV.create(num=18, bat_cap_kwh=35.8, wallbox_kw=11.1, bat_limit=80, eff=85,
                     mode='with_controller', soc_old=70)

ev_1 = evsim.EV.create(num=18, bat_cap_kwh=50, wallbox_kw=12, bat_limit=85, eff=90,
                       mode='with_controller', soc_old=60)

users_energy = ubsim_energy.User.create(18)
users_plug = ubsim_plug.User.create(18)

sun = DNIdata.Sun.create(18)
pvs = pvsim.PV.create(num=18, lat=32.2, area=34, efficiency=0.25, el_tilt=32.2, az_tilt=0.0)

sun_1 = DNIdata.Sun.create(18)
pvs_1 = pvsim.PV.create(num=18, lat=32.2, area=34, efficiency=0.25, el_tilt=32.2, az_tilt=0.0)


# Connect entities:

for i in range(len(houses)):
    world.connect(houses[i], nodes[i], 'P')

for i in range(len(users_energy)):
    world.connect(users_energy[i], ev[i], ('lost_energy_kwh', 'power'))

for i in range(len(users_energy)):
    world.connect(users_energy[i], ev_1[i], ('lost_energy_kwh', 'power'))

for i in range(len(users_plug)):
    world.connect(users_plug[i], ev[i], ('car_plugged_in', 'power'))

for i in range(len(users_plug)):
    world.connect(users_plug[i], ev_1[i], ('car_plugged_in', 'power'))

for i in range(len(users_energy)):
    world.connect(users_energy[i], ev[i], ('lost_energy_kwh', 'soc'))
    world.connect(users_plug[i], ev[i], ('car_plugged_in', 'soc'))

for i in range(len(users_energy)):
    world.connect(users_energy[i], ev_1[i], ('lost_energy_kwh', 'soc'))
    world.connect(users_plug[i], ev_1[i], ('car_plugged_in', 'soc'))

for i in range(len(sun)):
    world.connect(sun[i], pvs[i], 'DNI')

for i in range(len(sun_1)):
    world.connect(sun_1[i], pvs_1[i], 'DNI')

for i in range(len(pvs)):
    world.connect(pvs[i], ev[i], ('P', 'power'))

for i in range(len(pvs)):
    world.connect(pvs[i], ev[i], ('P', 'soc'))

for i in range(len(pvs_1)):
    world.connect(pvs_1[i], ev_1[i], ('P', 'power'))

for i in range(len(pvs_1)):
    world.connect(pvs_1[i], ev_1[i], ('P', 'soc'))

for i in range(len(pvs)):
    world.connect(pvs[i], nodes[i], 'P')

for i in range(len(pvs_1)):
    world.connect(pvs_1[i], nodes[i], 'P')

for i in range(len(ev)):
    world.connect(ev[i], nodes[i], ('power', 'P'))

for i in range(len(ev_1)):
    world.connect(ev_1[i], nodes[i], ('power', 'P'))


# Initializing and instantiating a database component:
db = world.start('DB', step_size=15*60, duration=END)
hdf5 = db.Database(filename='test_22.hdf5')
#hdf5 = db.Database(filename='EV_with.hdf5')


connect_many_to_one(world, ev, hdf5, 'soc')
connect_many_to_one(world, ev_1, hdf5, 'soc')
connect_many_to_one(world, nodes, hdf5, 'P')
connect_many_to_one(world, nodes, hdf5, 'Vm')
connect_many_to_one(world, ev, hdf5, 'power')
connect_many_to_one(world, ev_1, hdf5, 'power')


# Execute simulation:
world.run(until=END)

