
"""
Mosaik interface for the example simulator.

"""
import mosaik_api
import itertools
import arrow
import EV_charger_model

META = {
    'models': {
        'EV': {
            'public': True,
            'params': ['bat_cap_kwh',    # battery capacity
                       'wallbox_kw',     # wallbox power
                       'bat_limit',      # the limit of the EV battery
                       'eff',            # the EV charger efficiency
                       'soc_old',        # the old value of state of charge
                       'mode'            # with_controller or without_controller modes
                       ],
            'attrs': ['power',           # output power
                      'soc',             # output state of charge
                      'lost_energy_kwh', # input from user's behaviour
                      'car_plugged_in'   # input from user's behaviour
                      ]
        },
    },
}

DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'

class EVSim(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.sid = None
        self.cache = None
        self._entities = {} # contains the model instances
        self.eid_counters = {}


    def init(self, sid, start_date, step_size):
        self.sid = sid
        self.start_date = start_date
        self.step_size = step_size

        return self.meta


    def create(self, num, model, **model_params):
        counter = self.eid_counters.setdefault(model, itertools.count())

        entities = []

        for i in range(num):
            eid = '%s_%s' % (model, next(counter))
            self._entities[eid] = EV_charger_model.Model(start_date = self.start_date
                                                          , **model_params)
            entities.append({'eid': eid, 'type': model, 'rel': []})
        return entities


    def step(self, time, inputs):
        self.cache = {}
        for eid, attrs in inputs.items():
            self.cache[eid] = {}
            for attr, vals in attrs.items():

                if attr == 'power':
                    lost_energy_kwh = list(vals.items())[0][1]
                    car_plugged_in = list(vals.items())[1][1]
                    p = list(vals.items())[2][1]
                    self.cache[eid][attr] = self._entities[eid].power(lost_energy_kwh, car_plugged_in, p
                                                                      , step_size=self.step_size)
                    self._entities[eid].step_size(self.step_size)

                elif attr == 'soc':
                    lost_energy_kwh = list(vals.values())[0]
                    car_plugged_in = list(vals.values())[1]
                    p = list(vals.values())[2]
                    self.cache[eid][attr] = self._entities[eid].state_of_charge(lost_energy_kwh, car_plugged_in, p
                                                                                , step_size=self.step_size)
                    self._entities[eid].step_size(self.step_size)

        return time + self.step_size


    def get_data(self, outputs):
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self._entities.keys():
                raise ValueError('Unknown entity ID "%s"' % eid)

            data[eid] = {}
            for attr in attrs:
                data[eid][attr] = self.cache[eid][attr]

        return data


def main():
    mosaik_api.start_simulation(EVSim(), 'mosaik-EV simulator')


if __name__ == '__main__':
    main()

